## Items

RuneGraph scans a wide range of sources, trying to keep up to date with the newest updates so you never miss new releases - of course this is complimentary.

**Resource Path**

`{base_path}/{version}/items [/{__id} ? ]`


|Parameter   | Default                     | Required | Setter/Getter                           |
|------------|-----------------------------|----------|-----------------------------------------|
|`host`      | `https://api.runegraph.com` | Yes      |`Client:: [set / get] Host [ $host ? ]`  |
|`base_path` | `{host}/`                   | Yes      |
|`version`   | `Client::VERSION`           | Yes      |



**Operations**

- [Create Item](https://gitlab.com/runegraph/docs/api/blob/master/items.create.md)
- [Import Item](https://gitlab.com/runegraph/docs/api/blob/master/items.import.md)
- [Update Item](https://gitlab.com/runegraph/docs/api/blob/master/items.update.md)
- Retrieving Items
    - [Single Item](https://gitlab.com/runegraph/docs/api/blob/master/items.get.single.md)
    - [List of Items](https://gitlab.com/runegraph/docs/api/blob/master/items.get.list.md)
    - [Range of Items](https://gitlab.com/runegraph/docs/api/blob/master/items.get.range.md)
    
    
    
...To be continued...
    