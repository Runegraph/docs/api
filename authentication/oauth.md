# Authentication
RuneGraph uses OAuth2.0 for authenticating clients, in order to use our API you need to request developer access and we have to review your project.
Please send a detailed email to: kristian@runegraph.com where you include the following information: 

- Legal Contact person (Full name, address, E-mail address (direct) and phone number.)
- Project URL / GitHub / GitLab
- Estimated monthly requests to our API (We need this to ensure we can handle your project and to assign quotas)
